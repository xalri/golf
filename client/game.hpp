#pragma once

#include "render/window.hpp"
#include "render/camera.hpp"
#include "model/world.hpp"
#include "model/message.hpp"
#include "net/socket.hpp"
#include "xal_prof.h"

struct Game {
    enum class State { Idle, Joining, Playing } state = State::Idle;

    Channel<Message> outgoing;
    Sequencer<Ev, Req> sequencer {};
    World world {};
    SocketAddr server_addr{};
    bool sent_load_resp = false;
    std::optional<float> hit_start {};

    explicit Game(Channel<Message> outgoing): outgoing(std::move(outgoing)) {}

    /// Attempt to join a game
    void join(const char* hostname, uint16_t port) {
        std::cout << "Attempting to join server at " << hostname << std::endl;

        if (zed_net_get_address(&server_addr, hostname, port) == -1) {
            std::cerr << "Failed to resolve server's hostname: " << zed_net_get_error() << "\n";
            exit(1);
        }

        std::cout << "Resolved server address as " << server_addr << std::endl;
        state = State::Joining;

        // Construct the join request
        auto msg = make_msg(server_addr, GuaranteeLevel::Order, MessageData::Kind::Join);
        msg.data->join.kind = Join::Kind::Request;
        msg.data->join.request = Join::Request { "xal" };

        outgoing->enqueue(std::move(msg));
    }

    /// Handle an incoming message
    void recv(Message msg) {

        if (msg.data->kind == MessageData::Kind::Join) {
            if (msg.data->join.kind == Join::Kind::Disconnect) {
                std::cerr << "Disconnected" << std::endl;
                exit(1);
            }
            if (msg.data->join.kind == Join::Kind::Load) {
                std::cout << "Got load request.." << std::endl;

                if (state != State::Joining) {
                    // TODO: reset world, ready for the next map
                    sent_load_resp = false;
                    state = State::Joining;
                }

                auto counter = msg.data->join.load.counter;
                auto res = msg.data->join.load.resource;

                auto resp = make_msg(server_addr, GuaranteeLevel::Order, MessageData::Kind::Join);
                resp.data->join.kind = Join::Kind::LoadResp;
                resp.data->join.load_resp = Join::LoadResp { res };
                outgoing->enqueue(std::move(resp));

                sequencer.counter = counter;
                sent_load_resp = true;
            }
        }
        else if (msg.data->kind == MessageData::Kind::GameEvs) {
            if (msg.addr != server_addr) {
                std::cerr << "Got a game message from unknown source\n";
                return;
            }
            if (state == State::Joining) {
                if (sent_load_resp) {
                    std::cout << "Connected to server, probably" << std::endl;
                    state = State::Playing;
                    auto test_req = Req{ Req::Kind::Test };
                    sequencer.add_event(Req::rule{}, test_req);
                } else {
                    std::cerr << "Got a game message before Join::Load, ignoring\n";
                }
            }

            if (state == State::Playing) {
                auto up_to_date = sequencer.receive(msg.data->game_evs);
                apply_world_evs();
                // TODO: don't update if packet is a duplicate..
                auto _p = prof("World::update");
                world.update();
            }
        }
    }

    /// Handle incoming events
    void apply_world_evs() {
        auto _p = prof("Game::apply_world_evs");
        auto prev_group = std::optional<Seq>{};
        std::optional<Ev> ev {};
        do {
            // Get the next event
            ev = sequencer.next_ev(Ev::rule{});
            // Get the event group this event belongs to
            auto group = sequencer.dispatch_group_id();

            // Update the world for any completed event groups (may be multiple if groups are empty)
            // TODO: this is flawed, there is NOT a 1-1 relationship between event groups & ticks,
            // and `sequencer::dispatch` is nullified at the end of a group, so this is always applied
            // for the last ev group in a message ._.
            // if (prev_group.has_value() && group != *prev_group) world.update();

            if (ev.has_value()) {
                world.apply_ev(*ev);
            }

            prev_group = { group };
        } while(ev.has_value());
    }

    /// Get the index of the player controlled golf ball.
    std::optional<size_t> ball_idx() {
        std::optional<size_t> idx;
        if (world.player_no.has_value()) {
            for (auto i = 0u; i < world.balls.entries.size(); i++)
                if (world.balls.contains(i) && world.balls[i].player_no == *world.player_no) idx = { i };
        }
        return idx;
    }

    /// Handle a window event
    void on_window_ev(WindowEv const& ev, Window const& window, Camera const& camera) {
        auto ball_idx = this->ball_idx();

        if (ev.kind == WindowEvKind::KeyPress) {
            if (ev.key.code == GLFW_KEY_R && ball_idx.has_value()) {
                auto req = Req { Req::Kind::UpdateBall };
                req.update_ball.ball_id = (uint16_t) *ball_idx;
                req.update_ball.body = BodyNet {
                    /*position    */ {2, 5.0, 0},
                    /*linear_vel  */ v3{ -0.04f, -0.1f, 0.0f } * 0.5f,
                    /*orientation */ r3(),
                    /*angular_vel */ v3(0.0),
                };
                sequencer.add_event(Req::rule{}, req);
            }
        }
        if (ev.kind == WindowEvKind::MouseButtonPress) {
            hit_start = { ev.mouse.y };
        }
        if (state == State::Playing && ev.kind == WindowEvKind::MouseButtonRelease) {
            if (ball_idx.has_value() && ev.mouse.button == 1) {
                auto power = ((*hit_start - (float)ev.mouse.y) / (float)window.size.y) * 0.1f;
                power = clamp(power, 0.0f, 0.035f);
                auto dir = camera.front;
                dir.y = 0;

                auto req = Req { Req::Kind::PushBall };
                req.push_ball.ball_id = (uint16_t) *ball_idx;
                req.push_ball.direction = dir;
                req.push_ball.magnitude = power;
                sequencer.add_event(Req::rule{}, req);
            }
        }
    }

    /// Update the game
    void update() {
        // Send queued requests
        if (state == State::Playing) {
            auto msg = make_msg(server_addr, GuaranteeLevel::None, MessageData::Kind::GameReqs);
            msg.data->game_reqs = sequencer.make_msg();
            outgoing->enqueue(std::move(msg));
        }
    }
};
