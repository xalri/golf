#pragma once

#include "model/join.hpp"
#include "client.hpp"

/// A place for clients while they're loading a map.
struct Limbo {
    struct Soul {
        Client client;
        uint64_t keepalive_in;
        uint64_t keepalive_out;
    };

    struct Lost {
        enum class Reason { Disconnected, Timeout } reason;
        Client client;
    };

    Channel<Message> outgoing;
    MapDescriptor current_map {};
    std::vector<Soul> souls {};
    std::vector<Lost> lost_souls {};
    std::vector<Client> ready {};

    explicit Limbo(Channel<Message> outgoing) : outgoing(std::move(outgoing)) { }

    /// Updates Limbo; call this every tick.
    /// Passes completed clients to `on_join` and lost clients (with the
    /// disconnection reason) to `on_lost`.
    template<class OnJoin, class OnLost>
    void update(OnJoin on_join, OnLost on_lost) {
        auto time = now();

        for (auto& l : lost_souls) on_lost(std::move(l));
        lost_souls.clear();

        for (auto& c : ready) on_join(std::move(c));
        ready.clear();

        for (auto it = souls.begin(); it != souls.end();) {
            auto& s = *it;
            if (time - s.keepalive_in > seconds_to_ns(20.0)) {
                // Send disconnect message
                auto msg = make_msg(s.client.addr, GuaranteeLevel::Order, MessageData::Kind::Join);
                msg.data->join.kind = Join::Kind::Disconnect;
                outgoing->enqueue(std::move(msg));

                on_lost(Lost{ Lost::Reason::Timeout, std::move(it->client) });
                souls.erase(it);
                continue;
            }

            if (time - s.keepalive_out > seconds_to_ns(2.0)) {
                auto msg = make_msg(s.client.addr, GuaranteeLevel::Order, MessageData::Kind::Join);
                msg.data->join.kind = Join::Kind::KeepAlive;
                outgoing->enqueue(std::move(msg));
                s.keepalive_out = time;
            }
            ++it;
        }
    };

    /// Add a new client
    void add_client(Client client) {
        auto time = now();
        auto soul = Soul { std::move(client), time, time };

        auto addr = soul.client.addr;
        std::cout << "Sending load request to " << addr << "\n";

        soul.client.load(current_map);

        auto msg = make_msg(soul.client.addr, GuaranteeLevel::Order, MessageData::Kind::Join);
        msg.data->join.kind = Join::Kind::Load;
        msg.data->join.load = Join::Load{ soul.client.counter, current_map };

        outgoing->enqueue(std::move(msg));

        souls.push_back(std::move(soul));
    }

    /// Process an incoming message.
    void recv(Message const &msg) {
        if (msg.data->kind != MessageData::Kind::Join) return;

        auto it = souls.begin();
        while (it != souls.end()) {
            if (it->client.addr == msg.addr) break;
            else ++it;
        }
        if (it == souls.end()) return;

        if (msg.data->join.kind == Join::Kind::Disconnect) {
            std::cout << "Client disconnected while loading (" << msg.addr << ")\n";
            lost_souls.push_back(Lost{ Lost::Reason::Disconnected, std::move(it->client) });
            souls.erase(it);
        }
        else if (msg.data->join.kind == Join::Kind::KeepAlive) {
            it->keepalive_in = now();
        }
        else if (msg.data->join.kind == Join::Kind::LoadResp) {
            auto client = std::move(it->client);
            souls.erase(it);

            if (msg.data->join.load_resp.resource != current_map) {
                std::cout << "Map changed while client was connecting, resending load request\n";
                add_client(std::move(client));
            } else {
                std::cout << "Client load complete (" << msg.addr << ")\n";
                ready.push_back(std::move(client));
            }
        }
    }
};
