#pragma once

#include <cstdint>
#include "net/encode.hpp"

/// A ping message
struct Ping {
    enum class Kind { Ping, Pong, Count } kind;
    uint32_t id;

    using rule = all_of<
        field_rule<&Ping::kind, enum_rule<(uint64_t) Kind::Count>>,
        field_rule<&Ping::id, bits_rule<uint32_t, 32>>
    >;
};

/// Ping responses are stored in a circular buffer -- only the newest responses
/// are kept. BUFFER_LEN defines the size of this buffer.
const size_t BUFFER_LEN = 20;

/// The time in milliseconds between pings to each client/server.
const float PING_TIME_MS = 300.0;

/// The time after which to automatically stop pinging an unresponsive client/server.
const uint64_t TIMEOUT_NS = 10'000'000'000;

/// The time before we consider a ping lost.
const uint64_t LOST_NS = 800'000'000;

/*
/// Ping tracking
struct Pings {
    /// Maps socket addresses to the individual ping trackers.
    std::vector<PingClient> clients;
    /// A channel on which to send messages to clients.
    Channel<Message> sender;
    size_t ping_counter;
    size_t ping_idle;
    uint64_t last_update;
};

 struct PingClient {
    // ...
 };

 */
