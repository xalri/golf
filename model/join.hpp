#pragma once

#include "net/encode.hpp"

struct MapDescriptor {
    std::string name;
    // TODO: file size & checksum to uniquely identify different versions of maps, etc

    bool operator==(const MapDescriptor &rhs) const {
        return name == rhs.name;
    }

    bool operator!=(const MapDescriptor &rhs) const {
        return !(rhs == *this);
    }

    using rule = field_rule<&MapDescriptor::name, string_rule>;
};

// Join handshake:
//  client: Request{ nickname }
//  server: Denied { reason } OR Load { some map }
//  if not denied:
//    client: <while loading map...> KeepAlive
//    after loading map:
//    client: LoadResp { some map }
//  if the map has changed:
//    server: Load { the new map }
//    ... repeat as above until the client manages to load a map before it changes ._.
//  otherwise the server sends a game message with synchronisation events.
//  eventually:
//    client: Disconnect
//  or:
//    server: Denied { <kicked by a server admin | ...> }
struct Join {
    enum class Kind {
        Request, Denied, Load, LoadResp, KeepAlive, Disconnect, Count
    } kind;

    struct Request { std::string nickname; };
    struct Denied { std::string reason; };
    struct Load { uint8_t counter; MapDescriptor resource; };
    struct LoadResp { MapDescriptor resource; };

    Request request {};
    Denied denied {};
    Load load {};
    LoadResp load_resp {};
    uint8_t keepalive = 0;
    uint8_t disconnect = 0;

    using rule = variant_rule<
        field_rule<&Join::kind, enum_rule<(uint64_t) Kind::Count>>,
        field_rule<&Join::request, all_of<
            field_rule<&Request::nickname, string_rule>
        >>,
        field_rule<&Join::denied, all_of<
            field_rule<&Denied::reason, string_rule>
        >>,
        field_rule<&Join::load, all_of<
            field_rule<&Load::counter, bits_rule<uint8_t, 1>>,
            field_rule<&Load::resource, MapDescriptor::rule>
        >>,
        field_rule<&Join::load_resp, all_of<
            field_rule<&LoadResp::resource, MapDescriptor::rule>
        >>,
        field_rule<&Join::keepalive, empty_rule>,
        field_rule<&Join::disconnect, empty_rule>
    >;
};