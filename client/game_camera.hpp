#pragma once

#include "render/window.hpp"
#include "render/camera.hpp"
#include "xal_math.h"

struct GameCamera {
    Camera camera{};
    float arcball_dist = 6.0f;
    bool free = false;

    GameCamera() {
        camera.fovy = deg2rad(30.0f);
        camera.position = v3 { 14.0f, 2.9f, -6.0f };
        camera.yaw = 2.617f;
        camera.pitch = -0.152f;
    }

    void on_ev(WindowEv const& ev) {
        if (ev.kind == WindowEvKind::MouseWheelScroll) {
            arcball_dist = clamp(arcball_dist - (float) ev.mouse_wheel.y_off * 0.12f, 0.5f, 15.0f);
        }
        if (ev.kind == WindowEvKind::KeyPress) {
            if (ev.key.code == GLFW_KEY_C) free = !free;
        }
    }

    void update(Window const& window, float dt, std::optional<v3> target) {
        auto camera_speed = 0.01f * dt;
        if (window.key_pressed[GLFW_KEY_W]) camera.move_forward(camera_speed);
        if (window.key_pressed[GLFW_KEY_S]) camera.move_forward(-camera_speed);
        if (window.key_pressed[GLFW_KEY_A]) camera.move_left(camera_speed);
        if (window.key_pressed[GLFW_KEY_D]) camera.move_left(-camera_speed);

        if (window.mouse_pressed[0] || window.mouse_pressed[1]) {
            camera.yaw += window.mouse_diff.x * 0.002f;
        }
        if (window.mouse_pressed[0]) {
            camera.pitch -= window.mouse_diff.y * 0.002f;
        }

        if (window.key_pressed[GLFW_KEY_P]) {
            std::cout << "Camera position: " << camera.position << " yaw: " << camera.yaw << " pitch: " << camera.pitch << std::endl;
        }

        if (!free && target.has_value()) camera.arcball(*target, arcball_dist);

        camera.update();
    }

};
