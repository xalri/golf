#version 450 core

#pragma include glsl/light_common.glsl

void main() {
    // Unpack G-buffer
    Fragment frag = current_frag();

    // fragment -> camera vector
    vec3 v = normalize(eye_position - frag.world_pos);

    vec3 light_accum = vec3(0.0);

    // basic volumetric absorbtion & scattering
    const float scatter_coef = 0.0009; // amount of light to scatter per unit length
    const float extinguish_coef = 0.005; // amount of light to absorb per unit length
    const float ray_length = 23.0;
    const int samples = 256;
    const vec3 scatter_step = vec3(ray_length * scatter_coef / samples);
    const float extinguish_step = 1.0 - (ray_length * extinguish_coef / samples);

    vec3 sample_pos = eye_position;
    vec3 step = -v * ray_length / samples;
    float max_depth = length(frag.view_pos);

    for (int i = 0; i < samples; i++) {
        light_accum *= extinguish_step;

        for (int i = 0; i < n_spot_lights; i++) {
            SpotLight l = spot_lights[i];
            float vis = spot_light_visibility(sample_pos, l, true);
            light_accum += 3.0 * l.colour * vis * scatter_step;
        }

        for (int i = 0; i < n_sun_lights; i++) {
            SunLight l = sun_lights[i];
            float vis = sun_light_visibility(sample_pos, l, false);
            light_accum += 0.8 * l.colour * vis * scatter_step;
        }

        if (length(sample_pos - eye_position) > max_depth) break;
        sample_pos += step;
    }

    out_col = vec4(light_accum, 1.0);
}