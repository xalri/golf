#pragma once

#include "model/world.hpp"
#include "model/message.hpp"
#include "net/socket.hpp"

struct ClientID {
    std::string nickname;
};

/// Handles communications with a single client.
struct Client {
    ClientID id {};
    uint8_t counter = 0;
    /// A number identifying the player within a game.
    uint16_t player_no = 0;
    /// The client's address
    SocketAddr addr {};

    /// The player's currently loading resource
    MapDescriptor resource {};

    /// Sequences events.
    Sequencer<Req, Ev> sequencer {};
    /// The number of game ticks for which the client has been connected.
    uint64_t client_tick {};
    /// The number of updates written since we last received a message.
    uint64_t unresponsive {};
    /// The time we last sent a `Tick` event.
    uint64_t tick_sync_timer {};

    /// Start loading a map, discards any current state.
    void load(MapDescriptor map) {
        counter += 1;
        resource = std::move(map);
    }

    /// Join the game.
    void play() {
        sequencer = Sequencer<Req, Ev>{};
        sequencer.counter = counter;
        client_tick = 0;
        unresponsive = 0;
        tick_sync_timer = now();
    }

    /// Handle an incoming message
    void recv(GameReqs data) {
        unresponsive = 0;
        auto up_to_date = sequencer.receive(data);
        // TODO: transient state?
    }

    /// Parse and take the next incoming event from the sequencer, returns `None` if there are none.
    std::optional<Req> next_ev() {
        return sequencer.next_ev(Req::rule{});
    }

    /// Send an event to this client.
    void send_ev(Ev ev) {
        try {
            sequencer.add_event(Ev::rule{}, ev);
        } catch(std::exception const& e) {
            std::cerr << "Failed to encode outgoing event: " << e.what() << "\n";
        }
    }

    /// Create the next message to be sent to the client.
    GameEvs tick() {
        unresponsive += 1;
        return sequencer.make_msg();
    }

    /// Check if the client is unresponsive.
    bool is_unresponsive() {
        return !sequencer.alive || unresponsive > 300;
    }
};
