#version 450 core

struct Attrs {
    vec4 col_factor;
    vec3 emissive_factor;
    float metallic_factor;
    float roughness_factor;
    int col_tx;
    int mr_tx;
    int ao_tx;
    int normal_tx;
    int emissive_tx;
    uint _padding[2];
};

uniform sampler2DArray tx;
layout(std430, binding = 2) buffer attr_buf { Attrs attrs[]; };
layout(std430, binding = 3) buffer tx_size_buf { vec2 tx_size[]; };

in Vertex {
    flat uint attrs_idx;
    vec3 normal;
    vec2 uv;
    vec3 world_pos;
} IN;

vec4 get_texel(in int layer, in vec2 uv) {
    if (layer < 0) {
        return vec4(1.0);
    } else {
        return texture(tx, vec3(uv * tx_size[layer], layer));
    }
}

void main() {
    Attrs at = attrs[IN.attrs_idx];
    float a = get_texel(at.col_tx, IN.uv).a * at.col_factor.a;
    if (a < 0.05) discard;
}