#version 450 core
#extension GL_ARB_shader_draw_parameters : enable

struct FireParticle {
    vec3 position;
    bool alive;
    vec3 velocity;
    float life;
    float life_drain;
    uint kind;
};

layout(std430, binding = 0) buffer fire_buf { FireParticle fire[]; };

uniform mat4 view;
uniform mat4 model;

out Vertex {
    flat float life;
    flat uint kind;
    flat mat3 uv_matrix;
} OUT;

void main() {
    FireParticle p = fire[gl_VertexID];

    float angle = p.life * 6.0;
    if (p.kind % 2 == 1) angle = -angle;
    float c = cos(angle);
    float s = sin(angle);
    OUT.uv_matrix = mat3(c, -s, 0.0, s, c, 0.0, (1.0 - c - s) * 0.5, (1.0 + s - c) * 0.5, 1.0);

    OUT.life = p.life;
    OUT.kind = p.kind;
    vec4 pos = view * model * vec4(p.position, 1.0);
    gl_Position = pos;
    gl_PointSize = 250.0 / pos.z;
}
