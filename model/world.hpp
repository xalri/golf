#pragma once

#include <vector>
#include "net/encode.hpp"
#include "render/gltf.hpp"
#include "xal_math.h"
#include "xal_slab.h"
#include "xal_collide.h"
#include "xal_nav.h"
#include "xal_bvt.h"

/// The length in nanoseconds of a single game tick.
static const uint64_t TICK_TIME_NS = 1'000'000'000ull / 60ull;

/// A rule for encoding a v3.
template<int min, int max, int scale>
using v3_rule = all_of <
        field_rule<&v3::x, int_rule<min, max, scale>>,
        field_rule<&v3::y, int_rule<min, max, scale>>,
        field_rule<&v3::z, int_rule<min, max, scale>>
>;

/// A rule for encoding a normalized r3.
template<int scale>
using r3_rule = all_of <
        field_rule<&r3::w, int_rule< -scale, scale, scale>>,
        field_rule<&r3::x, int_rule< -scale, scale, scale>>,
        field_rule<&r3::y, int_rule< -scale, scale, scale>>,
        field_rule<&r3::z, int_rule< -scale, scale, scale>>
>;

/// World configuration.
struct WorldConfig {
    // TODO: gravity, ball collision, elasticity modifier, ...
    using rule = empty_rule;
};

/// A player in the game.
struct Player {
    uint16_t player_id = 0;
    std::string nickname {};
    std::vector<size_t> scores {};

    using rule = all_of<
        field_rule<&Player::player_id, u16_rule>,
        field_rule<&Player::nickname, string_rule>
    >;
};

/// A golf ball
struct GolfBall {
    Body body {};
    float radius = 0.13f;
    constexpr static float density = 5.0f;
    uint16_t player_no;

    GolfBall(Body body, uint16_t player_no, float radius = 0.13f):
        body(body), player_no(player_no), radius(radius)
    {
        update_mass();
    }

    /// Get the ball's collision sphere.
    constexpr Sphere collision_sphere() const {
        return Sphere{ radius, body.position, body.linear_vel };
    }

    /// Update mass & inertia based on the ball's radius
    constexpr void update_mass() {
        body.inv_mass = invert(mass(collision_sphere()) * density);
        body.inv_inertia = (inertia(collision_sphere()) * density).invert();
    }
};

/// An obstacle in the game world
struct Obstacle {
    Body body {};
    std::vector<Triangle> tris;
    bool is_death = false;
    size_t scene_node; // index to the node in the scene graph
    v3 scale;
    m4 transform {};
    std::vector<Triangle> world_tris {}; // collision tris in world space

    void update() {
        transform = make_transform(body.position, scale, body.orientation);
        world_tris.clear();
        for (auto& t : tris) world_tris.push_back({ transform * t.a, transform * t.c, transform * t.b } );
    }
};

/// Transient state of a body
struct BodyNet {
    v3 position {};
    v3 linear_vel {};
    r3 orientation {};
    v3 angular_vel {};

    static BodyNet from_body(Body const& b) {
        return BodyNet { b.position, b.linear_vel, b.orientation, b.angular_vel };
    }

    void to_body(Body &b) {
        b.position = position;
        b.linear_vel = linear_vel;
        b.orientation = orientation;
        b.angular_vel = angular_vel;
    }

    using rule = all_of <
        field_rule<&BodyNet::position, v3_rule< -10'000'000, 10'000'000, 10000 >>,
        field_rule<&BodyNet::linear_vel, v3_rule< -100'000, 100'000, 1000 >>,
        field_rule<&BodyNet::orientation, r3_rule< 1000 >>,
        field_rule<&BodyNet::angular_vel, v3_rule< -100'000, 100'000, 1000 >>
    >;
};

/// A discrete change in game state, authored by the server.
/// When a client joins mid-game, the server sends the minimal set of events which re-creates
/// the current world state.
struct Ev {
    enum class Kind {
        /// Create or update a player.
        CreatePlayer,
        /// Removes a player from the game given their ID.
        RemovePlayer,
        /// Spawn a golf ball in the game world.
        CreateBall,
        /// Apply some force to a ball.
        PushBall,
        /// Remove a ball from the world.
        RemoveBall,
        /// Start playing some hole.
        StartHole,
        /// End a hole, gives the player's scores for this hole
        EndHole,
        /// Override the state of a ball (e.g. to move back to it's previous position
        /// when it lands out of bounds)
        SyncBall,
        /// Update the world's configuration
        UpdateWorldConfig,
        /// Sent after all synchronisation events, tells the client that the world
        /// is in a valid state.
        SyncComplete,
        Count,
    } kind;

    constexpr static const char* kind_str[(size_t) Kind::Count] = {
        "CreatePlayer", "RemovePlayer", "CreateBall", "PushBall", "RemoveBall",
        "StartHole", "EndHole", "SyncBody", "UpdateWorldConfig", "SyncComplete"
    };

    struct CreateBall {
        uint16_t ball_id = 0;
        uint16_t player_id = 0;
        BodyNet body;

        using rule = all_of<
            field_rule<&CreateBall::ball_id, u16_rule>,
            field_rule<&CreateBall::player_id, u16_rule>,
            field_rule<&CreateBall::body, BodyNet::rule>
        >;
    };

    struct RemovePlayer {
        uint16_t id = 0;
        std::string reason {};

        using rule = all_of<
            field_rule<&RemovePlayer::id, u16_rule>,
            field_rule<&RemovePlayer::reason, string_rule>
        >;
    };

    struct PushBall {
        uint16_t ball_id = 0;
        v3 direction {};
        float magnitude;

        using direction_rule = v3_rule < -1000, 1000, 1000 >;

        using rule = all_of<
            field_rule<&PushBall::ball_id, bits_rule<uint16_t, 16>>,
            field_rule<&PushBall::direction, direction_rule>,
            field_rule<&PushBall::magnitude, int_rule<0, 5000, 100'000>>
        >;
    };

    struct RemoveBall {
        uint16_t ball_id = 0;
        using rule = field_rule<&RemoveBall::ball_id, u16_rule>;
    };

    struct StartHole {
        uint16_t hole_id = 0;
        using rule = field_rule<&StartHole::hole_id, u16_rule>;
    };

    struct EndHole {
        std::vector<uint16_t> scores{};
        using rule = field_rule<&EndHole::scores, vec_rule<bits_rule<uint16_t, 16>>>;
    };

    struct SyncBall {
        uint16_t ball_id = 0;
        BodyNet body;

        using rule = all_of<
                field_rule<&SyncBall::ball_id, u16_rule>,
                field_rule<&SyncBall::body, BodyNet::rule>
        >;
    };

    struct SyncComplete {
        uint32_t tick = 0;
        uint16_t player_no = 0;
        using rule = all_of<
            field_rule<&SyncComplete::tick, bits_rule<uint32_t, 32>>,
            field_rule<&SyncComplete::player_no, u16_rule>
        >;
    };

    Player create_player {};
    RemovePlayer remove_player {};
    CreateBall create_ball {};
    PushBall push_ball {};
    RemoveBall remove_ball {};
    StartHole start_hole {};
    EndHole end_hole {};
    SyncBall sync_ball {};
    WorldConfig update_world_config {};
    SyncComplete sync_complete {};

    using rule = variant_rule<
            field_rule<&Ev::kind, enum_rule<(uint64_t) Kind::Count>>,
            field_rule<&Ev::create_player, Player::rule>,
            field_rule<&Ev::remove_player, RemovePlayer::rule>,
            field_rule<&Ev::create_ball, CreateBall::rule>,
            field_rule<&Ev::push_ball, PushBall::rule>,
            field_rule<&Ev::remove_ball, RemoveBall::rule>,
            field_rule<&Ev::start_hole, StartHole::rule>,
            field_rule<&Ev::end_hole, EndHole::rule>,
            field_rule<&Ev::sync_ball, SyncBall::rule>,
            field_rule<&Ev::update_world_config, WorldConfig::rule>,
            field_rule<&Ev::sync_complete, SyncComplete::rule>
    >;
};

/// A request from a client to change something in the world
struct Req {
    enum class Kind { PushBall, UpdateBall, Test, Count } kind;

    Ev::PushBall push_ball;
    Ev::SyncBall update_ball;
    uint8_t test = 0;

    using rule = variant_rule<
            field_rule<&Req::kind, enum_rule<(uint64_t) Kind::Count>>,
            field_rule<&Req::push_ball, Ev::PushBall::rule>,
            field_rule<&Req::update_ball, Ev::SyncBall::rule>,
            field_rule<&Req::test, u8_rule>
    >;
};

/// The world state.
struct World {
    uint64_t tick = 0;
    uint16_t current_hole = 0;
    slab<Player> players {};
    slab<GolfBall> balls {};
    std::optional<uint16_t> player_no {};
    std::vector<Anim> animations {};

    // Derived state, not directly influenced by events
    std::vector<Obstacle> static_obstacles {};
    std::vector<Obstacle> dynamic_obstacles {};
    std::vector<std::pair<size_t, Obstacle const*>> contacts;
    Bvt static_bvt {};
    Bvt dynamic_bvt {};
    NavGraph nav_mesh;

    /// Initialise from a GLTF scene.
    void load_scene(Scene const& scene);

    /// Get the index of the ball controlled by some player.
    std::optional<size_t> ball_idx(uint16_t player_no) const;

    /// Apply an event to the world.
    void apply_ev(Ev &ev);

    /// Update the world.
    void update();
    void update_obstacles();
    void collide();
};
