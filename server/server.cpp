#include <thread>
#include <chrono>
#include "net/socket.hpp"
#include "model/world.hpp"
#include "model/message.hpp"
#include "xal_time.h"

#include "client.hpp"
#include "limbo.hpp"

using namespace std::chrono_literals;

struct Server {
    Channel<Message> outgoing;
    Limbo joining;
    std::vector<Client> playing {};
    uint16_t next_player_id = 1;
    World world{};

    explicit Server(Channel<Message> const& outgoing): outgoing(outgoing), joining(outgoing) {}

    /// Handle an incoming message
    void recv(Message const& msg) {
        joining.recv(msg);

        auto it = playing.begin();
        while (it != playing.end()) {
            if (it->addr == msg.addr) break;
            else ++it;
        }
        auto is_known_client = it != playing.end();

        if (msg.data->kind == MessageData::Kind::Join) {
            if (msg.data->join.kind == Join::Kind::Request) {
                std::cout << "Got join request from " << msg.addr << std::endl;
                if (is_known_client) {
                    std::cerr << "Got duplicate join request, removing player" << std::endl;
                    // TODO: remove player event
                    playing.erase(it);
                }

                auto client = Client{};
                client.id.nickname = msg.data->join.request.nickname;
                client.addr = msg.addr;
                // `joining` handles load requests, keepalives, etc, then passes the client
                // back to us when they're ready to play.
                joining.add_client(std::move(client));
            }
            else if (msg.data->join.kind == Join::Kind::Disconnect) {
                std::cerr << "Got disconnect message from " << msg.addr << "\n";

                if (is_known_client) {
                    auto ev = Ev{};
                    ev.kind = Ev::Kind::RemovePlayer;
                    ev.remove_player = {it->player_no, "Left the game"};
                    global_ev(ev);

                    playing.erase(it);
                }
            }
        }
        else if (msg.data->kind == MessageData::Kind::GameReqs) {
            if (!is_known_client) {
                std::cerr << "Got game message from unknown client " << msg.addr << "\n";
                // Send disconnect message
                auto dc = make_msg(msg.addr, GuaranteeLevel::Order, MessageData::Kind::Join);
                dc.data->join.kind = Join::Kind::Disconnect;
                outgoing->enqueue(std::move(dc));
            } else {
                // Take incoming requests
                it->recv(msg.data->game_reqs);
            }
        }
        else if (msg.data->kind == MessageData::Kind::Ping) {
            // TODO
        }
        else {
            std::cerr << "Unexpected message type\n";
        }
    }

    void sync_balls() {
        for (auto i = 0u; i < world.balls.entries.size(); i++) {
            if (!world.balls.contains(i)) continue;
            auto b = world.balls[i];
            auto q_ev = Ev { Ev::Kind::SyncBall };
            q_ev.sync_ball = Ev::SyncBall { (uint16_t) i, BodyNet::from_body(b.body) };
            global_ev(q_ev);
        }
    }

    /// Update the game
    void update() {
        auto resync = world.tick % 10 == 0;

        // Add new clients
        joining.update(
            [&](Client new_client) {
                new_client.player_no = next_player_id;
                new_client.play();
                next_player_id += 1;

                std::cout << "Adding client to game ("
                    << new_client.addr << ", player_id " << new_client.player_no << ")" << std::endl;

                playing.push_back(std::move(new_client));
                auto& client = playing.back();

                for (auto& p : world.players) {
                    auto ev = Ev{Ev::Kind::CreatePlayer};
                    ev.create_player = p;
                    client.send_ev(ev);
                }

                for (auto i = 0u; i < world.balls.entries.size(); i++) {
                    if (!world.balls.contains(i)) continue;
                    auto& b = world.balls[i];
                    auto ev = Ev{Ev::Kind::CreateBall};
                    ev.create_ball = Ev::CreateBall {
                        (uint16_t) i,
                        b.player_no,
                        BodyNet::from_body(b.body)
                    };
                    client.send_ev(ev);
                }

                // Quantise ball for all clients:
                resync = true;

                auto player = Player { client.player_no, client.id.nickname };

                {
                    auto ev = Ev{Ev::Kind::CreatePlayer};
                    ev.create_player = player;
                    global_ev(ev);
                }

                {
                    auto ev = Ev{Ev::Kind::SyncComplete};
                    ev.sync_complete = {(uint32_t) world.tick, client.player_no};
                    client.send_ev(ev);
                }

                {
                    auto ev = Ev{Ev::Kind::CreateBall};
                    ev.create_ball = {
                            (uint16_t) world.balls.reserve(),
                            client.player_no,
                            BodyNet {
                                { 2, 5.0, 0 }, v3{ -0.04f, -0.1f,  0.0f } * 0.5f
                            }
                    };
                    global_ev(ev);
                }

                // TODO: sync client:
                //  - create players (including themselves)
                //  - create balls
                //  - current state of all dynamic bodies?
                //  - send synccomplete
            },
            [&](Limbo::Lost lost) {
                std::cerr << lost.client.addr << " disconnected while loading\n";
            }
        );

        if (resync) sync_balls();

        // Check for unresponsive clients
        for (auto it = playing.begin(); it != playing.end();) {
            if (it->is_unresponsive()) {
                std::cerr << "Dropping unresponsive client " << it->addr << "\n";
                // We don't send a disconnect message here,

                // Add a RemovePlayer event
                auto ev = Ev {};
                ev.kind = Ev::Kind::RemovePlayer;
                ev.remove_player = { it->player_no, "Connection lost" };
                global_ev(ev);

                // Remove the client
                playing.erase(it);
            } else {
                ++it;
            }
        }

        // Handle incoming events
        for (auto& c : playing) {
            std::optional<Req> req {};
            while((req = c.next_ev()).has_value()) {

                if (req->kind == Req::Kind::PushBall) {
                    // TODO: validation
                    auto ev = Ev { Ev::Kind::PushBall };
                    ev.push_ball = req->push_ball;
                    global_ev(ev);
                }
                
                if (req->kind == Req::Kind::UpdateBall) {
                    auto ev = Ev { Ev::Kind::SyncBall };
                    ev.sync_ball = req->update_ball;
                    global_ev(ev);
                }

            }
        }

        // Forward new events to clients
        for (auto& c : playing) {
            auto msg = make_msg(c.addr, GuaranteeLevel::None, MessageData::Kind::GameEvs);
            msg.data->game_evs = c.tick();
            outgoing->enqueue(std::move(msg));
        }

        world.update();
    }

    void global_ev(Ev const& ev) {
        auto _ev = decode<Ev>(Ev::rule{}, encode(Ev::rule{}, ev));
        world.apply_ev(_ev);
        for (auto& c : playing) c.send_ev(_ev);
    }
};

#include "render/window.hpp"
#include "render/gltf.hpp"
// TODO:
#include "../client/render.hpp"
#include "../client/game_camera.hpp"

int main() {
    auto window = Window("hai", 1280, 720);
    window.set_vsync(0);
    auto ctx = gl::Context{};

    auto socket = Socket<MessageData, MessageData::rule>::bind(27270, MessageData::rule{});
    auto outgoing = std::make_shared<spsc_bounded_queue_t<Message>>(64);

    auto scene = Scene {};
    scene.import_gltf("./resources/forest.gltf");

    auto server = Server(outgoing);
    server.world.load_scene(scene);

    //auto render = Renderer(ctx, std::move(scene));
    //auto camera = GameCamera();

    auto timer = StepTimer(TICK_TIME_NS);

    bool done = false;
    while(!done) {
        window.poll_events();

        while(auto ev = window.next_event()) {
            //camera.on_ev(*ev);
            //render.on_ev(*ev, window);
        }

        socket.connections.update();

        // Forward raw messages to the connection manager.
        channel_recv(socket.raw_rx, [&](auto msg) { socket.connections.recv(msg); });

        // Take incoming messages & forward to the game
        channel_recv(socket.incoming_rx, [&](auto msg) { server.recv(msg); });

        timer.tick([&]() {
            server.update();
        });

        // Send outgoing messages
        channel_recv(outgoing, [&](auto msg) { socket.connections.send(msg); });

        std::this_thread::sleep_for(std::chrono::nanoseconds(timer.step / 10));

        window.clear(v4(0.0, 0.0, 0.0, 1.0), 1.0);
        //camera.update(window, 16.0f, { });
        //render.draw(window, server.world, camera.camera);
        window.display();
    }
}
