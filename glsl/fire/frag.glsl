#version 450 core

layout(location=0) out vec3 out_emission;
uniform sampler2DArray tx;

in Vertex {
    flat float life;
    flat uint kind;
    flat mat3 uv_matrix;
} IN;

void main() {
    vec2 tile_size = textureSize(tx, 0).xy / 8.0;
    vec2 uv = (IN.uv_matrix * vec3(gl_PointCoord * 0.7 + 0.15, 1.0)).xy;
    uv = (uv / 8.0) + vec2(IN.kind * tile_size.x, 0.0);
    float value = texture(tx, vec3(uv, 0)).r;
    if (IN.life < value) discard;
    //out_emission = vec3(IN.life);
    out_emission = 5.0 * vec3(0.3, 0.09 * IN.life, 0.05 * IN.life) * pow(IN.life, 2.0) * (1.0 - value);
}
