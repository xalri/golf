set(SOURCE_FILES
        world.hpp
        world.cpp
        join.hpp
        ping.hpp
        message.hpp
        )
add_library(model STATIC ${SOURCE_FILES})
target_link_libraries(model xal_libs net render)
target_include_directories(model PUBLIC ../)
