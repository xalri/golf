#version 450 core
#extension GL_ARB_shader_draw_parameters : enable

struct Object {
    uint mesh;
    uint transform;
    uint attrs;
};

layout(location=0) uniform mat4 view;

layout(location=0) in vec3 v_position;
layout(location=1) in vec3 v_norm;
layout(location=2) in vec2 v_uv;

out Vertex {
    flat uint attrs_idx;
    vec3 normal;
    vec2 uv;
    vec3 world_pos;
} OUT;

layout(std430, binding = 0) buffer object_buf { Object objects[]; };
layout(std430, binding = 1) buffer transform_buf { mat4 transforms[]; };

void main() {
    Object o = objects[gl_DrawIDARB];
    mat4 t = transforms[o.transform];
    mat3 norm_t = inverse(transpose(mat3(t)));
    OUT.attrs_idx = o.attrs;
    OUT.normal = norm_t * v_norm;
    OUT.uv = v_uv;
    OUT.world_pos = vec3(t * vec4(v_position, 1.0));
    gl_Position = view * (t * vec4(v_position, 1.0));
}
