#pragma once

#include "net/guarantee.hpp"
#include "net/sequencer.hpp"
#include "join.hpp"
#include "ping.hpp"

using GameReqs = SequentialData;
using GameEvs = SequentialData;

/// A top-level game message
struct MessageData {
    enum class Kind {
        Join, Ping, GameReqs, GameEvs, Count
    } kind;

    Join join;
    Ping ping;
    GameReqs game_reqs; // client => server
    GameEvs game_evs; // server => client

    using rule = variant_rule<
        field_rule<&MessageData::kind,      enum_rule<(uint64_t) Kind::Count>>,
        field_rule<&MessageData::join,      Join::rule>,
        field_rule<&MessageData::ping,      Ping::rule>,
        field_rule<&MessageData::game_reqs, SequentialData::rule>,
        field_rule<&MessageData::game_evs,  SequentialData::rule>
    >;
};

using Message = Packet<MessageData>;

Message make_msg(SocketAddr addr, GuaranteeLevel level, MessageData::Kind kind) {
    auto msg = Message{};
    msg.addr = addr;
    msg.level = level;
    msg.data = { MessageData{} };
    msg.data->kind = kind;
    return msg;
}
