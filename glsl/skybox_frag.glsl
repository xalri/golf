#version 450 core

uniform samplerCubeArray cube_maps;
uniform int env_map_idx;

in vec3 local_pos;
out vec3 out_emission;

void main() {
    out_emission = clamp(texture(cube_maps, vec4(normalize(local_pos), env_map_idx)).xyz, vec3(0.0), vec3(1000.0));
    gl_FragDepth = 1.0;
}
