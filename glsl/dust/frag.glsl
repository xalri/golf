#version 450 core

layout(location=0) out vec4 out_col;
layout(location=1) out vec3 out_emission;
layout(location=2) out vec3 out_norm;
layout(location=3) out vec3 out_mro;

in Vertex {
    vec3 normal;
} IN;

void main() {
    out_col = vec4(0.01);
    out_emission = vec3(0.2);
    out_norm = IN.normal;
    out_mro = vec3(0.04, 0.8, 1.0);
}

