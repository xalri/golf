#version 450 core
#extension GL_ARB_shader_draw_parameters : enable

struct DustParticle {
    vec3 position;
    bool alive;
    vec3 velocity;
    int _pad1;
    vec3 orientation;
    int _pad2;
};

layout(std430, binding = 0) buffer dust_buf { DustParticle dust[]; };

uniform mat4 view;

out Vertex {
    vec3 normal;
} OUT;

void main() {
    DustParticle p = dust[gl_VertexID];
    vec3 n = p.orientation;
    if (dot(mat3(view) * n, vec3(0, 0, -1)) < 0.0) n = -n;

    OUT.normal = n;
    vec4 pos = view * vec4(p.position, 1.0);
    gl_Position = pos;
    //gl_PointSize = 1.0 / pos.z;
}
