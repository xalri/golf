#include <map>
#include "world.hpp"
#include "xal_time.h"
#include "render/gltf.hpp"

void build_bvt(Bvt& bvt, std::vector<Obstacle> const& obstacles) {
    bvt.clear();
    for (auto i = 0; i < obstacles.size(); i++) {
        auto& o = obstacles[i];
        auto _id = i << 16;
        for (auto j = 0; j < o.world_tris.size(); j++) {
            auto tri = o.world_tris[j];
            bvt.add_node(bounds3::from_triangle(tri.a, tri.b, tri.c), (int32_t) (_id | j));
        }
    }
}

void World::load_scene(const Scene &scene) {
    for (auto& o : scene.objs) {
        auto is_nav = o.name.find("nav") == 0;
        if (is_nav) {
            auto &prim = scene.meshes[o.mesh].primitives[0];
            auto points = std::vector<v3>{};
            auto m = scene.scene_graph.nodes[o.scene_node].local_transform();
            for (auto& v : prim.verts) points.push_back(m * v.pos);
            nav_mesh = NavGraph::build(prim.indices, points);
        }
        if (o.collidable) {
            auto const& node = scene.scene_graph.nodes[o.scene_node];
            auto is_death = o.name.find("death") == 0;
            for (auto& p : scene.meshes[o.mesh].primitives) {
                auto obstacle = Obstacle {
                        Body {},
                        p.collision_tris,
                        is_death,
                        o.scene_node,
                        node.scale,
                };
                obstacle.body.position = node.translation;
                obstacle.body.orientation = node.rotation;
                obstacle.body.inv_mass = 0.0;
                obstacle.body.inv_inertia = m3(0.0);
                obstacle.body.elasticity = o.elasticity;
                obstacle.body.friction = o.friction;

                obstacle.update();

                if (o.dynamic) dynamic_obstacles.push_back(obstacle);
                else static_obstacles.push_back(obstacle);
            }
        }
    }

    build_bvt(static_bvt, static_obstacles);

    for (auto& a : scene.animations) {
        auto anim = Anim{};
        for (auto& c : a.channels) {
            auto idx = std::optional<size_t>{};
            for (auto i = 0; i < dynamic_obstacles.size(); i++) {
                if (dynamic_obstacles[i].scene_node == c.target.node) {
                    idx = { i };
                    break;
                }
            }

            if (idx.has_value()) {
                auto channel = c;
                channel.target.node = *idx;
                anim.channels.push_back(channel);
            }
        }
        if (!anim.channels.empty()) animations.push_back(anim);
    }
}

std::optional<size_t> World::ball_idx(uint16_t player_no) const {
    std::optional<size_t> idx;
    for (auto i = 0u; i < balls.entries.size(); i++)
        if (balls.contains(i) && balls[i].player_no == player_no) idx = { i };
    return idx;
}

void World::apply_ev(Ev &ev) {
    //std::cout << "Applying " << Ev::kind_str[(size_t)ev.kind] << " event, tick " << tick << std::endl;

    auto validate = [](bool predicate, const char* message) {
        if (!predicate) {
            std::cerr << message << "\n";
            exit(1);
        }
    };

    if (ev.kind == Ev::Kind::SyncComplete) {
        tick = ev.sync_complete.tick;
        player_no = { ev.sync_complete.player_no };
    }

    if (ev.kind == Ev::Kind::CreateBall) {
        auto new_ball = ev.create_ball;
        auto b = new_ball.body;

        validate(players.contains(new_ball.player_id),
                 "Invalid CreateBall event, player ID out of range");

        validate(!balls.contains(new_ball.ball_id),
                 "Invalid CreateBall event, ball ID already in use");

        balls.insert_at(new_ball.ball_id, GolfBall {
                Body { b.position, b.linear_vel, b.orientation, b.angular_vel },
                new_ball.player_id
        });
    }

    if (ev.kind == Ev::Kind::SyncBall) {
        auto new_ball = ev.sync_ball;
        auto b = new_ball.body;

        validate(balls.contains(new_ball.ball_id),
                 "Invalid SyncBall event, ball ID unknown");

        b.to_body(balls[new_ball.ball_id].body);
    }

    if (ev.kind == Ev::Kind::PushBall) {
        auto idx = (size_t) ev.push_ball.ball_id;

        validate(balls.contains(idx),
                 "Invalid PushBall event, ball ID out of range");

        auto& body = balls[idx].body;
        body.linear_vel = body.linear_vel + ev.push_ball.direction * ev.push_ball.magnitude * body.inv_mass;
    }

    if (ev.kind == Ev::Kind::CreatePlayer) {
        auto idx = (size_t) ev.create_player.player_id;

        validate(!players.contains(idx),
                 "Invalid CreatePlayer event, player ID already in use");

        players.insert_at(idx, std::move(ev.create_player));
    }

    if (ev.kind == Ev::Kind::RemovePlayer) {
        auto idx = ev.remove_player.id;

        validate(players.contains(idx),
                 "Invalid RemovePlayer event, player ID out of range");

        auto ball_idx = this->ball_idx(idx);
        if (ball_idx.has_value()) {
            balls.remove(*ball_idx);
        }
    }
}

void World::update() {
    collide();
    update_obstacles();
    for (auto &b : balls) b.body.integrate();
    for (auto &b : balls) b.body.linear_vel.y -= 0.004;
    tick += 1;
}

void World::update_obstacles() {
    auto time = tick * ns_to_seconds(TICK_TIME_NS);

    for (auto& o : dynamic_obstacles) {
        o.body.linear_vel = v3{};
        o.body.angular_vel = v3{};
    }

    for (auto& a : animations) {
        for (auto &c : a.channels) {
            auto &o = dynamic_obstacles[c.target.node];

            auto _v = c.value((float) time);
            if (!_v.has_value()) continue;
            auto v = *_v;

            if (c.target.property == Anim::Target::Property::Translation) {
                auto old_position = o.body.position;
                o.body.position = v3(v);
                auto diff = o.body.position - old_position;
                o.body.linear_vel = diff;
            }
            if (c.target.property == Anim::Target::Property::Rotation) {
                auto old_orientation = o.body.orientation;
                o.body.orientation = r3{v.x, v.y, v.z, v.w};
                auto c = o.body.orientation * old_orientation.invert();
                o.body.angular_vel = c.to_euler();
            }
        }
    }
}

void World::collide() {
    for (auto& o : dynamic_obstacles) o.update();
    build_bvt(dynamic_bvt, dynamic_obstacles);

    // Sphere-sphere collision
    for (auto i = 0u; i < balls.entries.size(); i++) {
        if (!balls.contains(i)) continue;
        for (auto j = i + 1; j < balls.entries.size(); j++) {
            if (!balls.contains(j)) continue;
            auto &ba = balls[i];
            auto &bb = balls[j];
            auto sa = ba.collision_sphere();
            auto sb = bb.collision_sphere();

            auto c = ::collide(sa, sb);
            if (c.intersection.time.has_value()) {
                auto res = resolve(c, ba.body, bb.body);
                res.first.apply(ba.body);
                res.second.apply(bb.body);
            }
        }
    }

    // Sphere-world collision
    contacts.clear();
    std::map<size_t, Contact> minimum_contacts {};

    auto i = 0;
    for (auto& b : balls) {
        auto s = b.collision_sphere();

        auto run = [&](Bvt const& bvt, std::vector<Obstacle> const& obstacles) {
            bvt.query(s.bounds(), [&](int32_t id) {
                auto obstacle_id = id >> 16;
                auto tri_id = id & ((1 << 16) - 1);

                auto c = ::collide(s, obstacles[obstacle_id].world_tris[tri_id]);

                if (c.has_value()) {
                    auto &min_c = minimum_contacts[obstacle_id];
                    min_c = Contact::min(min_c, c);
                }
                return true;
            });

            for (auto&[obstacle_id, min_c] : minimum_contacts) {
                auto &o = obstacles[obstacle_id];
                min_c.support_b = min_c.support_b - o.body.position;
                auto res = resolve(min_c, b.body, o.body);
                res.first.apply(b.body);

                contacts.emplace_back(i, &o);
            }

            minimum_contacts.clear();
        };

        run(static_bvt, static_obstacles);
        run(dynamic_bvt, dynamic_obstacles);
        i++;
    }
}
