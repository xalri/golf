#pragma once

#include "render/light.hpp"
#include "render/colour.hpp"
#include "render/draw.hpp"
#include "render/gltf.hpp"
#include "render/window.hpp"
#include "render/cubemap.hpp"
#include "render/debug.hpp"
#include "render/camera.hpp"
#include "render/text.hpp"
#include "model/world.hpp"

/// Draws a game world.
struct Renderer {
    gl::Context &ctx;
    Scene scene;
    gl::DrawParameters params {};
    DDRender<Window> dd_render { ctx }; // debug draw

    RenderTexture<1> surface_buffer { 1280, 720, { GL_RGB16F }, false, 1, true };
    RenderTexture<4> g_buffer { 1280, 720, { GL_RGB8, GL_RGB8, GL_RGB16F, GL_RGB16F }, true, 1, false, true };

    gl::Program geom_program          = gl::Program::vert_frag("glsl/deferred_vert.glsl",  "glsl/deferred_frag.glsl");
    gl::Program skybox_program        = gl::Program::vert_frag("glsl/cube_vert.glsl",      "glsl/skybox_frag.glsl");
    gl::Program shadow_program        = gl::Program::vert_frag("glsl/deferred_vert.glsl",  "glsl/depth_frag.glsl");
    gl::Program surface_light_program = gl::Program::vert_frag("glsl/fill_vert.glsl",      "glsl/surface_light.glsl");
    gl::Program volume_light_program  = gl::Program::vert_frag("glsl/fill_vert.glsl",      "glsl/volume_light.glsl");
    gl::Program cc_program            = gl::Program::vert_frag("glsl/fill_vert.glsl",      "glsl/cc_frag.glsl");

    gl::TextureCubeArray cube_maps = load_dds_cubemap("./resources/output_skybox.dds");

    Font font = Font { "resources/DejaVuSans.ttf" };

    gl::Elem<SunLight> sun_light {};

    gl::Elem<MeshData<Vertex>> golf_ball_mesh {};
    gl::Elem<Material> golf_ball_mat {};

    constexpr static size_t MAX_GOLF_BALLS = 64;
    // A bunch of slots in the scene graph reserved for golf balls.
    std::array<size_t, MAX_GOLF_BALLS> golf_ball_transforms {};
    gl::Buffer<RenderObject<Vertex, Material>> golf_ball_buf { MAX_GOLF_BALLS, nullptr, gl::BufferType::MappedWrite };
    Batch<Vertex, Material> golf_ball_batch { MAX_GOLF_BALLS };

    // TODO: move object buf to inside batch, since it can't be reused anyway?
    std::optional<gl::Buffer<RenderObject<Vertex, Material>>> static_obj_buf {};
    std::optional<Batch<Vertex, Material>> static_batch {}; // optional for delayed initialisation.

    RenderTexture<0> shadow_map { 2048, 2048, { }, true };
    v3 light_inv = v3(0.5f, 0.9f, 0.5f).normalize();
    m4 shadow_proj = ortho(-10.0f, 20.0f, -10.0f, 20.0f, -30.0f, 30.0f);
    m4 shadow_view = look_at(light_inv, v3(0, 0, 0), v3(0, 1, 0));
    m4 shadow_pv = shadow_proj * shadow_view;

    explicit Renderer(gl::Context &ctx, Scene&& _scene): ctx(ctx), scene(std::move(_scene)) {
        dd::initialize(&dd_render);

        scene.import_gltf("./resources/golf_ball.gltf", false);
        // Assume the golf ball is the last element of the gltf scene...
        golf_ball_mesh = { scene.storage.mesh.head - 1 };
        golf_ball_mat = { scene.storage.material.head - 1 };

        for (auto i = 0; i < MAX_GOLF_BALLS; i++) {
            golf_ball_transforms[i] = scene.scene_graph.nodes.size();
            scene.scene_graph.nodes.emplace_back();
        }

        params.framebuffer_srgb = true;
        params.cubemap_seamless = true;
        params.depth_write = true;

        shadow_map.depth->set_params(gl::SamplerParams::nice());

        // Build a render batch for static scene geometry
        auto static_objs = std::vector<RenderObject<Vertex, Material>>{};

        for (auto& o : scene.objs) {
            if (!o.visible) continue;
            for (auto& p : scene.meshes[o.mesh].primitives) {
                static_objs.push_back(RenderObject<Vertex, Material> {
                        *p.mesh_data_idx,
                        gl::Elem<m4> { (uint32_t) (o.scene_node) },
                        p.material
                });
            }
        }

        static_obj_buf.emplace(static_objs.size(), static_objs.data(), gl::BufferType::Static);

        static_batch.emplace(static_objs.size());
        static_batch->build(*static_obj_buf, scene.storage.mesh.buf);

        scene.update_graph(); // builds the transform buffer from the scene graph..

        scene.lights.light_probes.elem(LightProbe { /*.map_idx =*/ 0, /*.factor  =*/ 0.5 });

        auto _sun_light = SunLight {
                /*.direction      =*/ light_inv,
                /*.has_shadow     =*/ GL_TRUE,
                /*.colour         =*/ temp_to_rgb(5500.f) * 6.0f,
                /*.shadow_map_idx =*/ 0,
                /*.shadow_pv      =*/ shadow_pv
        };

        sun_light = scene.lights.sun_lights.elem(_sun_light);
    }

    void on_ev(WindowEv const& ev, Window const& window) {
        // Resize fbo textures when the window changes size
        if (ev.kind == WindowEvKind::FramebufferResize) {
            surface_buffer.size = window._view_size;
            g_buffer.size = window._view_size;
            surface_buffer.init_fb();
            g_buffer.init_fb();
        }
    }

    void draw(Window& window, World const& world, Camera const& camera) {
        // Update scene graph from the game world
        for (auto& o : world.dynamic_obstacles) {
            auto& node = scene.scene_graph.nodes[o.scene_node];
            node.translation = o.body.position;
            node.rotation = o.body.orientation;
        }

        // Build the golf ball render batch
        assert(world.balls.len <= MAX_GOLF_BALLS);
        golf_ball_buf.size = (GLuint) world.balls.len;
        auto i = 0;
        for (auto &b : world.balls) {
            golf_ball_buf.data[i] = {
                golf_ball_mesh, { (uint32_t) golf_ball_transforms[i] }, golf_ball_mat
            };
            scene.scene_graph.nodes[golf_ball_transforms[i]] = Node {
                b.body.position,
                b.body.orientation,
                v3(b.radius * 2.0f),
            };
            i ++;
        }
        golf_ball_batch.build(golf_ball_buf, scene.storage.mesh.buf);

        scene.update_graph();

        dd_render.target = &window;
        dd_render.pv = camera.pv;

        g_buffer.clear({ v4(0.0, 0.0, 0.0, 1.0), v4(0.0, 0.0, 0.0, 0.0), v4(0.0, 0.0, 0.0, 0.0) }, 1.0);
        shadow_map.clear({}, 1.0);

        // Draw the scene to the gbuffer
        params.enable_depth = true;
        params.enable_culling = true;
        params.depth_test = gl::DepthTest::Less;
        static_batch->draw(
                ctx, g_buffer, scene.storage.vert.buf, scene.storage.idx.buf, geom_program,
                gl::PrimitiveType::Triangles, params, {
                    { "view", camera.pv },
                    { "tx", scene.storage.textures },
                    { "eye_position", camera.position },
                    { "object_buf", *static_obj_buf },
                    { "transform_buf", scene.transforms.buf },
                    { "attr_buf", scene.storage.material.buf },
                    { "tx_size_buf", scene.storage.tx_size.buf },
                });

        golf_ball_batch.draw(
                ctx, g_buffer, scene.storage.vert.buf, scene.storage.idx.buf, geom_program,
                gl::PrimitiveType::Triangles, params, {
                    { "view", camera.pv },
                    { "tx", scene.storage.textures },
                    { "eye_position", camera.position },
                    { "object_buf", golf_ball_buf },
                    { "transform_buf", scene.transforms.buf },
                    { "attr_buf", scene.storage.material.buf },
                    { "tx_size_buf", scene.storage.tx_size.buf },
                });

        // Draw the scene to the shadow map.
        static_batch->draw(
                ctx, shadow_map, scene.storage.vert.buf, scene.storage.idx.buf, shadow_program,
                gl::PrimitiveType::Triangles, params, {
                    { "view", shadow_pv },
                    { "tx", scene.storage.textures },
                    { "object_buf", *static_obj_buf },
                    { "transform_buf", scene.transforms.buf },
                    { "attr_buf", scene.storage.material.buf },
                    { "tx_size_buf", scene.storage.tx_size.buf },
                });

        golf_ball_batch.draw(
                ctx, shadow_map, scene.storage.vert.buf, scene.storage.idx.buf, shadow_program,
                gl::PrimitiveType::Triangles, params, {
                    { "view", shadow_pv },
                    { "tx", scene.storage.textures },
                    { "object_buf", golf_ball_buf },
                    { "transform_buf", scene.transforms.buf },
                    { "attr_buf", scene.storage.material.buf },
                    { "tx_size_buf", scene.storage.tx_size.buf },
                });

        // Draw the skybox to the emission texture.
        params.depth_test = gl::DepthTest::LEqual;
        ctx.draw_empty(g_buffer, skybox_program, gl::PrimitiveType::TriangleStrip, 14, params, {
                { "view", camera.view },
                { "projection", camera.projection },
                { "cube_maps", cube_maps },
                { "env_map_idx", 0 },
        });

        // Lighting pass
        params.enable_depth = false;
        auto light_uniforms = gl::Uniforms {
                { "in_depth", *g_buffer.depth },
                { "in_col", *g_buffer.col[0] },
                { "in_emission", *g_buffer.col[1] },
                { "in_norm", *g_buffer.col[2] },
                { "in_mro", *g_buffer.col[3] },
                { "inv_perspective", camera.inv_projection },
                { "inv_view", camera.inv_view },
                { "shadow_maps", *shadow_map.depth },
                { "eye_position", camera.position },
                { "cube_maps", cube_maps },
                { "n_spot_lights", scene.lights.spot_lights.size() },
                { "n_tube_lights", scene.lights.tube_lights.size() },
                { "n_sun_lights", scene.lights.sun_lights.size() },
                { "n_probes", scene.lights.light_probes.size() },
                { "spot_light_buf", scene.lights.spot_lights.buf },
                { "tube_light_buf", scene.lights.tube_lights.buf },
                { "sun_light_buf" , scene.lights.sun_lights.buf },
                { "probe_buf", scene.lights.light_probes.buf },
        };
        ctx.draw_empty(surface_buffer, surface_light_program, gl::PrimitiveType::Triangles, 3, params, light_uniforms);

        // Generate mipmaps for weighted average depth & colour
        //glGenerateTextureMipmap(*g_buffer.depth);
        //glGenerateTextureMipmap(*surface_buffer.col[0]);

        glNamedFramebufferTextureLayer(surface_buffer.fbo, GL_DEPTH_ATTACHMENT, *g_buffer.depth, 0, 0);

        // Post processing (DOF, HDR, CC, bloom, ...)
        ctx.draw_empty(window, cc_program, gl::PrimitiveType::Triangles, 3, params, {
                { "surface_light", *surface_buffer.col[0] },
                { "in_depth", *g_buffer.depth },
        });

        // Draw player names
        for (auto& b : world.balls) {
            auto& nick =  world.players[b.player_no].nickname;
            auto text_pos = b.body.position;
            text_pos.y += 0.3;
            auto m = make_transform(text_pos, v3(1.0), r3::from_euler(v3(0, -M_PI / 2.0 - camera.yaw, 0)));
            font.draw_text(nick, ctx, window, params, camera.pv * m);
        }

        // Explicit finish because we don't properly synchronize the mapped transform buffer >_>
        glFinish();
    }
};

void debug_draw_world(World const& world) {
    // Draw spheres
    for (auto& b : world.balls) {
        auto s = Sphere{b.radius, b.body.position, b.body.linear_vel};
        auto bounds = s.bounds();
        auto min = bounds.min();
        auto max = bounds.max();
        dd::aabb(min, max, v3(0.0, 1.0, 0.0));
        dd::sphere(b.body.position, v3(1, 0.7, 0.5), b.radius, b.body.orientation);
    }

    // Debug draw obstacles
    auto draw_obstacle = [&](Obstacle const& o) {
        for (auto& tri : o.world_tris) {
            dd::line(tri.a, tri.b, v3(0.0, 1.0, 1.0));
            dd::line(tri.b, tri.c, v3(0.0, 1.0, 1.0));
            dd::line(tri.c, tri.a, v3(0.0, 1.0, 1.0));
        }
    };
    for (auto& o : world.static_obstacles) draw_obstacle(o);
    for (auto& o : world.dynamic_obstacles) draw_obstacle(o);

    // Debug draw BVH
    for (auto& node : world.static_bvt.nodes) {
        auto min = node.bounds.min();
        auto max = node.bounds.max();
        dd::aabb(min, max, v3(1.0, 1.0, 0.0));
    }

    // Debug draw nav mesh
    for (auto& a : world.nav_mesh.nodes) {
        for (auto& _b : a.connected) {
            auto& b = world.nav_mesh.nodes[_b];
            dd::line(a.position, b.position, v3(1.0, 1.0, 0.0));
        }
    }

    {
        // Test path finding
        auto path_start = v3(-11.6363, 0.832873, 6.67562);
        if (world.balls.len > 0) {
            path_start = world.balls[0].body.position;
        }
        auto test_path = world.nav_mesh.find_path(
                path_start,
                v3(5.98636, 0.283731, -0.0687682)
        );
        for (auto i = 0; i < test_path.size() - 1; i++) {
            dd::line(test_path[i], test_path[i + 1], v3(1.0, 0.0, 0.0));
        }
    }

    dd::flush();
}

