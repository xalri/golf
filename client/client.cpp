#include "render/gltf.hpp"
#include "render/window.hpp"
#include "net/socket.hpp"
#include "xal_time.h"
#include "xal_prof.h"

#include "game.hpp"
#include "game_camera.hpp"
#include "render.hpp"

int main() {
    auto socket = Socket<MessageData, MessageData::rule>::bind(0, MessageData::rule{});
    auto outgoing = std::make_shared<spsc_bounded_queue_t<Message>>(64);

    auto window = Window("hai", 1280, 720);
    window.set_vsync(0);
    auto ctx = gl::Context{};

    auto scene = Scene {};
    scene.import_gltf("./resources/forest.gltf");

    auto game = Game(outgoing);
    game.world.load_scene(scene);

    auto render = Renderer(ctx, std::move(scene));
    auto camera = GameCamera();

    game.join("localhost", 27270);

    auto tick_timer = StepTimer(TICK_TIME_NS);
    auto prev_time = now();
    auto game_time = 0.0f;

    while (!window.should_close()) {
        auto time = now();
        auto dt = (float) ns_to_ms(time - prev_time);
        game_time += dt;
        prev_time = time;

        window.poll_events();

        while(auto ev = window.next_event()) {
            camera.on_ev(*ev);
            render.on_ev(*ev, window);
            game.on_window_ev(*ev, window, camera.camera);
        }

        socket.connections.update();

        std::optional<Message> packet{};
        // Forward raw messages to the connection manager.
        while ((packet = socket.raw_rx->dequeue()).has_value()) {
            auto msg = std::move(packet.value());
            packet = {};
            socket.connections.recv(msg);
        }

        // Take incoming messages & forward to the game
        while ((packet = socket.incoming_rx->dequeue()).has_value()) {
            auto msg = std::move(packet.value());
            packet = {};
            game.recv(std::move(msg));
        }

        tick_timer.tick([&]() {
            auto _p = prof("Game::update");
            game.update();
        });

        // Send outgoing messages
        while ((packet = outgoing->dequeue()).has_value()) {
            auto msg = std::move(packet.value());
            packet = {};
            socket.connections.send(msg);
        }

        auto _p = prof("draw");
        window.clear(v4(0.0, 0.0, 0.0, 1.0), 1.0);

        auto follow = std::optional<v3>{};
        if (game.ball_idx().has_value()) {
            follow = { game.world.balls[*game.ball_idx()].body.position };
        }

        camera.update(window, dt, follow);
        render.draw(window, game.world, camera.camera);
        debug_draw_world(game.world);

        auto line = 0;
        for (auto&[_, p] : xal_prof_statics::global_profile.profiles) {
            auto m = translate(v3(200.0f, 20.0f + line * 28.f, 0.0f)) * scale(v3(70.0f, -70.0f, 1.f));
            render.font.draw_text(p.to_string(), ctx, window, render.params, window.ortho() * m, false);
            line += 1;
        }

        window.display();
    }

    dd::shutdown();
}
